$(document).ready(function(){
  
  var audioContext = null;
  var meter = null;
  var rafID = null;
  var canvasContext = null;
  
  canvasContext = $('.visualizer')[0].getContext("2d");
  
  // monkeypatch Web Audio
  window.AudioContext = window.AudioContext || window.webkitAudioContext;
  
  // grab an audio context
  audioContext = new AudioContext();
  
  try {
    // monkeypatch getUserMedia
    navigator.getUserMedia = 
        navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia;

      // ask for an audio input
      navigator.getUserMedia({
        "audio": {
          "mandatory": {
            "googEchoCancellation": "false",
            "googAutoGainControl": "false",
            "googNoiseSuppression": "false",
            "googHighpassFilter": "false"
          },
          "optional": []
        },
      }, gotStream, didntGetStream);
    } catch (e) {
      alert('getUserMedia threw exception :' + e);
    }
    
  function didntGetStream() {
    alert('Stream generation failed.');
  }
  
  var mediaStreamSource = null;
  
  function gotStream(stream) {
    // Create an AudioNode from the stream.
    mediaStreamSource = audioContext.createMediaStreamSource(stream);

    // Create a new volume meter and connect it.
    meter = createAudioMeter(audioContext);
    mediaStreamSource.connect(meter);

    // kick off the visual updating
    render();
  }
  
  function render(time) {
    
    if (meter.checkClipping()) {
      console.log('Clipping!');
      console.log(meter.volume);
      console.log(meter);
      $('body').css({
        background: 'red'
      });
      $('.site-wrapper').addClass('shake');
      $('.site-wrapper').addClass('shake-constant')
      click = true;
    } else {
      $('body').css({
        background: 'green'
      });
      click = false;
      $('.site-wrapper').removeClass('shake');    
      $('.site-wrapper').removeClass('shake-constant')
    }                               
    //console.log(meter.volume)
    //console.log(meter)
    rafID = window.requestAnimationFrame( render );
  }
  
});