//

socket_url += 'desktop/';

var _coordX=0,
    _coordY=0,
    _click=false,
    start=false;

socketOnClose = function(event) {
  if (event.wasClean) {
    alert('Соединение закрыто чисто');
  } else {
    alert('Обрыв соединения. Код: ' + event.code); // например, "убит" процесс сервера
  };
  // location.reload()
};

socketOnOpen = function() {
  alert("Соединение установлено.");
};

socketOnMessageInit = function(event) {
  document.title = '# ' + event.data + ' desktop';
  socket.onmessage = socketOnMessage;
  alert('http://screenshake.xyz/ Номер комнаты: '+event.data);
};

socketOnMessage = function(event) {
  // console.log(event.data);
  var data = JSON.parse(event.data);

  // presigion = 0.001

  // if (Math.abs(_coordX - data.aGX) > presigion) {
    _coordX = data.aGX;
  // } else {
    // _coordX = 0;
  // };
  // if (Math.abs(_coordY - data.aGY) > presigion) {
    _coordY = data.aGY;
  // } else {
    // _coordY = 0;
  // };
  _click = data.click
  start = true;
};


createSocket = function() {
  socket = new WebSocket(socket_url);
  // socket.onclose = socketOnClose;
  // socket.onopen = socketOnOpen;
  socket.onmessage = socketOnMessageInit;
};


$(document).ready(function(){

  createSocket();

});


  //Create a Pixi stage and renderer
  var stage = new PIXI.Container();
  var renderer = PIXI.autoDetectRenderer(
    window.innerWidth, window.innerHeight,
    {antialiasing: false, transparent: false, resolution: 1}
  );

  document.body.appendChild(renderer.view);


  //Здесь должны загружаться текстуры
  //var loader = new PIXI.AssetLoader(["images/beh.png"]);
  //loader.onComplete = setup;
  //loader.load();

  setup();

  var state, enemy_scins, explorer_texture, explorer_texture_attack, explorer
      healthBar, message, gameScene, gameOverScene, _enemy_max, _enemy_list, _dead_list, offset;

  function setup() {

	_enemy_max=10
	_enemy_list=[]
	_dead_list=[]
	enemy_scins=[]
	floor_scins=[]
	floor_cover_scins=[]

	offset = {
	  x:0,
	  y:0,
	  x1:0,
	  y1:0,
	  i:0,
	  summ: function(){
		return this.x+this.y
	  },
	  summ1: function(){
		return this.x1+this.y1
	  }
	}

	//Make the game scene and add it to the stage

	PIXI.Sprite.prototype.bringToFront = function() {
	  if (this.parent) {
		  var parent = this.parent;
		  parent.removeChild(this);
		  parent.addChild(this);
	  }
	}

	gameScene = new PIXI.Container();
	//gameScene.width = 90000;
	//gameScene.height = 90000;
	gameScene.position.x = -90000/2;
	gameScene.position.y = -90000/2;

	enemy_scins.push(new PIXI.Texture.fromImage("images/filthy-peasant-2.png"));
	enemy_scins.push(new PIXI.Texture.fromImage("images/filthy-peasant-1.png"));

	floor_scins.push(new PIXI.Texture.fromImage("images/texture-soil-3.jpg"));
	floor_scins.push(new PIXI.Texture.fromImage("images/texture-soil-4.jpg"));
	floor_scins.push(new PIXI.Texture.fromImage("images/texture-soil.jpg"));
	floor_scins.push(new PIXI.Texture.fromImage("images/texture-soil2.jpg"));

	floor_cover_scins.push(new PIXI.Texture.fromImage("images/texture-soil-5.png"));
	floor_cover_scins.push(new PIXI.Texture.fromImage("images/texture-stones.png"));


	var floor_texture = floor_scins[randomInt(0, floor_scins.length-1)];
	//new PIXI.Texture.fromImage("images/beh.png");
	floor = new PIXI.extras.TilingSprite(floor_texture, 650, 450);

	floor.position.x = 0;
	floor.position.y = 0;
	floor.tilePosition.x = 0;
	floor.tilePosition.y = 0;
	floor.width = 90000;
	floor.height = 90000;
	gameScene.addChild(floor);

	var cover_r = randomInt(0,2)
	if (cover_r==0){
	  var floor_cover1_texture = floor_cover_scins[randomInt(0, floor_cover_scins.length-1)];
	} else if (cover_r==1){
	  var floor_cover1_texture = floor_cover_scins[0];
	  var floor_cover2_texture = floor_cover_scins[1];
	  floor_cover2 = new PIXI.extras.TilingSprite(floor_cover2_texture, 500, 500);
	  gameScene.addChild(floor_cover2);
	  floor_cover2.position.x = 0;
	  floor_cover2.position.y = 0;
	  floor_cover2.tilePosition.x = 0;
	  floor_cover2.tilePosition.y = 0;
	  floor_cover2.width = 90000;
	  floor_cover2.height = 90000;
	}
	floor_cover1 = new PIXI.extras.TilingSprite(floor_cover1_texture, 500, 500);
	gameScene.addChild(floor_cover1);
	floor_cover1.position.x = 0;
	floor_cover1.position.y = 0;
	floor_cover1.tilePosition.x = 0;
	floor_cover1.tilePosition.y = 0;
	floor_cover1.width = 90000;
	floor_cover1.height = 90000;

	stage.addChild(gameScene);

	//Make the sprites and add them to the `gameScene`
	//var texture = PIXI.Texture.fromImage("images/beh.png");
	//Explorer
	explorer_texture = PIXI.Texture.fromImage("images/barbar.png")
	explorer_texture_attack = PIXI.Texture.fromImage("images/masterase-active.png")
	explorer = new PIXI.Sprite(explorer_texture);
	//console.log(explorer.width, explorer.height);
	explorer.x = gameScene.position.x*-1 + renderer.width/2 //- explorer.width/2;
	explorer.y = gameScene.position.y*-1 + renderer.height/2// - explorer.height/2;
	//explorer.x = renderer.width/2 - explorer.width/2;
	//explorer.y = renderer.height/2 - explorer.height/2;
	explorer.scale.x=0.5;
	explorer.scale.y=0.5;
	explorer.vx = 0;
	explorer.vy = 0;
	explorer.anchor.set(0.5, 1);

	gameScene.addChild(explorer);

	//explorer.addChild(explorer);

	//Create the health bar
	healthBar = new PIXI.Container();

	healthBar.position.set(0,0)
	stage.addChild(healthBar);
	//Create the black background rectangle
	var innerBar = new PIXI.Graphics();
	innerBar.beginFill(0x000000);
	innerBar.drawRect(0, 0, renderer.width, 25);
	innerBar.endFill();
	healthBar.addChild(innerBar);
	//Create the front red rectangle
	var outerBar = new PIXI.Graphics();
	outerBar.beginFill(0xFF3300);
	outerBar.drawRect(0, 0, renderer.width, 25);
	outerBar.endFill();
	healthBar.addChild(outerBar);
	healthBar.outer = outerBar;


	/*camera = new PIXI.Container();
	camera.x = 0;
	camera.y = 0;
	camera.width=renderer.width;
	camera.height=renderer.height;*/

	//stage.addChild(camera);

	//Create the `gameOver` scene
	gameOverScene = new PIXI.Container();
	stage.addChild(gameOverScene);
	//Make the `gameOver` scene invisible when the game first starts
	gameOverScene.visible = false;
	//gameScene.visible = false;


	 //Create the text sprite and add it to the `gameOver` scene
	message = new PIXI.Text(
	  "The End!",
	  {font: "64px Futura", fill: "white"}
	);
	message.x = 120;
	message.y = renderer.height / 2 - 32;
	gameOverScene.addChild(message);

	//Set the game state
	state = play;

	//Start the game loop
	gameLoop();
  }


function gameLoop(){
  //Loop this function 60 times per second
  requestAnimationFrame(gameLoop);
  //Update the current game state
  state();
  //Render the stage
  renderer.render(stage);
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

function makeEnemy() {
  var _r = randomInt(0, enemy_scins.length-1);
  var enemy = new PIXI.Sprite(enemy_scins[_r]);
  enemy.scale.x=0.3;
  enemy.scale.y=0.3;
  var _direction = randomInt(0,3);
  if (_direction == 0) {
    enemy.y = explorer.y-renderer.height/2 - 150;
    enemy.x = randomInt(explorer.x-renderer.width/2, explorer.x+renderer.width/2);
  } else if (_direction == 1) {
    enemy.y = randomInt(explorer.y-renderer.height/2, explorer.y+renderer.height/2);
    enemy.x = explorer.x-renderer.width/2 - 150;
  } else if (_direction == 2) {
    enemy.y = explorer.y+renderer.height/2+150;
    enemy.x = randomInt(explorer.x-renderer.width/2, explorer.x+renderer.width/2);
  } else if (_direction == 3) {
    enemy.y = randomInt(explorer.y-renderer.height/2, explorer.y+renderer.height/2);
    enemy.x = explorer.x+renderer.width/2+150;
  }
  enemy.hp = getRandomArbitrary(0.1,99);
  enemy.rv = getRandomArbitrary(-0.2,0.2);
  //console.log(enemy.rv);
  enemy.anchor.set(0.5, 0.5);

  enemy.hit_cooldown = 0

  _enemy_list.push(enemy);
  gameScene.addChild(enemy);
}

// var maxrv = 0.1;
var minrv = 0.001;

function explorerRotate() {

  if (_coordX === 0 && _coordY === 0) {
    return
  }

  var angle = Math.PI + Math.atan2(-explorer.vx, explorer.vy) - explorer.rotation;

  if (Math.abs(angle) > Math.PI) {
    angle = (2 * Math.PI - Math.abs(angle))*2*(-angle/Math.abs(angle));
  }

  angle = angle / Math.PI

  if (Math.PI > Math.abs(angle) > minrv) {
    explorer.rotation += angle
  }
}


function play() {
  //use the cat's velocity to make it move


  if (explorer.rotation > (2 * Math.PI)) {
    explorer.rotation -= (2 * Math.PI)
  } else if (explorer.rotation < 0) {
    explorer.rotation += (2 * Math.PI)
  }


  if (_enemy_list.length < _enemy_max && start==true) {
    makeEnemy()
  }

  explorer.vx = -_coordX*0.3
  explorer.vy = _coordY*0.3

  offset.x1 += explorer.vx
  offset.y1 += explorer.vy

  //console.log(offset.x1, offset.y1)
  //console.log(offset.x, offset.y)

  if (offset.summ1()>(offset.summ()+100)) {
    offset.x = offset.x1
    offset.y = offset.y1
    //console.log(offset.i);
    offset.i++
    _enemy_max += (4+offset.i);
	console.log(_enemy_max);
    //alert('!')
  }

  //console.log(offset_x);

  var nextX = explorer.getGlobalPosition().x + explorer.vx;
  var nextY = explorer.getGlobalPosition().y + explorer.vy;
  var floorX = explorer.vx;
  var floorY = explorer.vy;


  var explorerHit = false;

  explorer.bringToFront();

  for (i=0; i < _enemy_list.length; i++) {

    var _enemy = _enemy_list[i];

    var enemyV = 1;
    var enemyHitDistance = 100;
    var explorerHitDistance = 200;

    if (hitTestRectangle(_enemy, explorer, enemyHitDistance)) {

      enemyV = 0.5;
      _enemy.y += randomInt(-10,10)
      _enemy.x += randomInt(-10,10)
      //if(_enemy.hit == true) {
        //enemyV = -5;
      //}
    }

    if(_enemy.hit == true) {
      //enemyV = -5;
      _enemy.alpha = 0.9;
      _enemy.tint = 0xFF0000
      _enemy.hp-=1;
      if (_enemy.hp <= 0) {
        _enemy.alpha = 0.3;
        _dead_list.push(_enemy);
        _enemy_list.splice(i, 1);
      }

      if (_enemy.x<=explorer.x) {
        _enemy.x -= enemyV+_enemy.hit_cooldown
      } else {
        _enemy.x += enemyV+_enemy.hit_cooldown
      }

      if (_enemy.y<=explorer.y) {
        _enemy.y -= enemyV+_enemy.hit_cooldown
      } else {
        _enemy.y += enemyV+_enemy.hit_cooldown
      }


    } else {
      _enemy.tint = 0xFFFFFF

      if (_enemy.x<=explorer.x) {
        _enemy.x += enemyV+randomInt(-1,1)
      } else {
        _enemy.x -= enemyV+randomInt(-1,1)
      }

      if (_enemy.y<=explorer.y) {
        _enemy.y += enemyV+randomInt(-1,1)
      } else {
        _enemy.y -= enemyV+randomInt(-1,1)
      }

    }





    _enemy.rotation += _enemy.rv//+randomInt(-0.01,0.01)

    //_enemy.x -= explorer.x;
    //_enemy.y -= explorer.y;


    if (hitTestRectangle(_enemy, explorer, explorerHitDistance) && !_enemy.hit) {
      if (_click) {
        _enemy.hit = true;
        _enemy.hit_cooldown = 15
      }
      if (hitTestRectangle(_enemy, explorer, enemyHitDistance)) {
        // _enemy.hit = false;
        var explorerHit = true;
      }
    } else {
      _enemy.hit_cooldown -= 1;
      if (_enemy.hit_cooldown === 0) {
        _enemy.hit = false;
      }
    };
    _enemy.bringToFront();
  }

  if (_click) {
    //healthBar.outer.width -= 1;
    explorer.texture = explorer_texture_attack;
    explorer.rotation +=0.52;

  	$('body').addClass('shake');
    $('body').addClass('shake-constant');
  } else {
    explorer.texture = explorer_texture;
    explorerRotate();
    //weapon.x = explorer.x;
    //weapon.y = explorer.y;
    //weapon.rotation = explorer.rotation;
	$('body').removeClass('shake');
    $('body').removeClass('shake-constant')
  }



  //gameScene.x -= explorer.vx;
  //gameScene.y -= explorer.vy;

  var border = 400;
  //console.log(nextX)
  //console.log(renderer.width)
  if (nextX>=border && nextX <= renderer.width-border) {
    explorer.x += explorer.vx;
  } else {
    explorer.x += explorer.vx;
    gameScene.x -= explorer.vx;
  }

  if (nextY>=border && nextY <= renderer.height-border) {
    explorer.y += explorer.vy;
  } else {
    explorer.y += explorer.vy;
    gameScene.y -= explorer.vy;
  }

  if(explorerHit) {
    //Make the explorer semi-transparent
    explorer.alpha = 0.5;
    //Reduce the width of the health bar's inner rectangle by 1 pixel
    healthBar.outer.width -= 1;
  } else {
    //Make the explorer fully opaque (non-transparent) if it hasn't been hit
    explorer.alpha = 1;
  }

  if (healthBar.outer.width < 0) {
    state = end;
	window.location.href='/loser.html'
    message.setText("You lost!");
  }
};

function end() {
  gameScene.visible = false;
  gameOverScene.visible = true;
}

/*hitTestRectangle = function(r1, r2) {
return !(r2.x > (r1.x + r1.width) ||
           (r2.x + r2.width) < r1.x ||
           r2.y > (r1.y + r1.height) ||
           (r2.y + r2.height) < r1.y);
}*/


function hitTestRectangle(r1, r2, hit_rad) {


  //Define the variables we'll need to calculate
  var hit, combinedHalfWidths, combinedHalfHeights, vx, vy;
  //hit will determine whether there's a collision
  hit = false;
  //Find the center points of each sprite
  r1.centerX = r1.getGlobalPosition().x + r1.width / 2;
  r1.centerY = r1.getGlobalPosition().y + r1.height / 2;
  r2.centerX = r2.getGlobalPosition().x + r2.width / 2;
  r2.centerY = r2.getGlobalPosition().y + r2.height / 2;


  if (Math.sqrt(Math.pow((r1.centerX - r2.centerX), 2) + Math.pow((r1.centerY - r2.centerY), 2)) < hit_rad) {
    hit = true;
  // } else {
    // hit = false;
  }


  //Find the half-widths and half-heights of each sprite
  // r1.halfWidth = r1.width / 2;
  // r1.halfHeight = r1.height / 2;
  // r2.halfWidth = r2.width / 2;
  // r2.halfHeight = r2.height / 2;
  //Calculate the distance vector between the sprites
  // vx = r1.centerX - r2.centerX;
  // vy = r1.centerY - r2.centerY;
  //Figure out the combined half-widths and half-heights
  // combinedHalfWidths = r1.halfWidth + r2.halfWidth;
  // combinedHalfHeights = r1.halfHeight + r2.halfHeight;
  //Check for a collision on the x axis
  // if (Math.abs(vx) < combinedHalfWidths) {
    //A collision might be occuring. Check for a collision on the y axis
    // if (Math.abs(vy) < combinedHalfHeights) {
      //There's definitely a collision happening
      // hit = true;
    // } else {
      //There's no collision on the y axis
      // hit = false;
    // }
  // } else {
    //There's no collision on the x axis
    // hit = false;
  // }
  //`hit` will be either `true` or `false`
  return hit;
};


/*
var hitTestRectangle = function(s2, s1)
{


    var x1 = s1.position.x - (s1.width/2),
    y1 = s1.position.y - (s1.height/2),
    w1 = s1.width,
    h1 = s1.height,
    x2 = s2.position.x - ( s2.width / 2 ),
    y2 = s2.position.y - ( s2.height / 2 ),
    w2 = s2.width,
    h2 = s2.height;

    if (x1 + w1 > x2)
        if (x1 < x2 + w2)
            if (y1 + h1 > y2)
                if (y1 < y2 + h2)
                    return true;


    if(hitTestRectangle(s2, s1))
        return true;

    return false;
};*/

function randomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
