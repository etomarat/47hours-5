$(document).ready(function(){

  var audioContext = null;
  var meter = null;
  var rafID = null;
  var canvasContext = null;

  canvasContext = $('.visualizer')[0].getContext("2d");

  // monkeypatch Web Audio
  window.AudioContext = window.AudioContext || window.webkitAudioContext;

  // grab an audio context
  audioContext = new AudioContext();

  try {
    /*// monkeypatch getUserMedia
    navigator.getUserMedia =
        navigator.getUserMedia ||
        navigator.webkitGetUserMedia ||
        navigator.mozGetUserMedia;

      // ask for an audio input
      */
      navigator.mediaDevices.getUserMedia({
        audio: false,
        video: true
      }).then(gotStream).catch(didntGetStream);

    } catch (e) {
      alert('getUserMedia threw exception :' + e);
    }

  function didntGetStream(e) {
    alert('Stream generation failed.');
    alert(e.name)
  }

  var mediaStreamSource = null;

  function gotStream(stream) {
    // Create an AudioNode from the stream.
    mediaStreamSource = audioContext.createMediaStreamSource(stream);

    // Create a new volume meter and connect it.
    meter = createAudioMeter(audioContext);
    mediaStreamSource.connect(meter);

    // kick off the visual updating
    render();
  }

  function render(time) {

    if (meter.checkClipping()) {
      console.log('Clipping!');
      console.log(meter.volume);
      console.log(meter);
      $('body').css({
        background: 'red'
      });
      $('.site-wrapper').addClass('shake');
      $('.site-wrapper').addClass('shake-constant')
      click = true;
    } else {
      $('body').css({
        background: 'green'
      });
      click = false;
      $('.site-wrapper').removeClass('shake');
      $('.site-wrapper').removeClass('shake-constant')
    }
    //console.log(meter.volume)
    //console.log(meter)
    rafID = window.requestAnimationFrame( render );
  }

});
//

var click=false;

socketOnOpen = function() {
  // socket.send('hello');
  // alert("Соединение установлено.");

  if (window.DeviceMotionEvent == undefined) {
    //No accelerometer is present. Use buttons.
    // alert("no accelerometer");
  }
  else {
    // alert("accelerometer found");
    window.addEventListener("devicemotion", accelerometerUpdate, true);
  }

  function accelerometerUpdate(e) {
    var OS = ( navigator.userAgent.match(/(iPad|iPhone|iPod)/g) ? -1 : 1 );
    var aGX = event.accelerationIncludingGravity.x*OS;
    var aGY = event.accelerationIncludingGravity.y*OS;
    var aGZ = event.accelerationIncludingGravity.z*OS;
    //The following two lines are just to calculate a
    // tilt. Not really needed.
    xGPosition = Math.atan2(aGY, aGZ);
    yGPosition = Math.atan2(aGX, aGZ);

    $('body .info').html('\
      aX:'+aGX+'<br>\
      aY:'+aGY+'<br>\
      aZ:'+aGZ+'<br>\
      xPosition:'+xGPosition+'<br>\
      yPosition:'+yGPosition+'<br>\
    ');

    var data = {
      aGX: parseFloat(aGX.toFixed(3)),
      aGY: parseFloat(aGY.toFixed(3)),
      click: click
      // aGZ: aGZ,
      // xGPosition: xGPosition,
      // yGPosition: yGPosition
    }

    socket.send(JSON.stringify(data));

  };

  var iOS = /(iPad|iPhone|iPod)/g.test( navigator.userAgent );

  if (iOS) {
    $('button.click').show();
  };
};

socketOnClose = function(event) {
  if (event.wasClean) {
    alert('Соединение закрыто чисто');
  } else {
    alert('Обрыв соединения. Код: ' + event.code); // например, "убит" процесс сервера
  };
  location.reload()
  // createSocket()
};

createSocket = function() {
  socket = new WebSocket(socket_url);
  socket.onclose = socketOnClose;
  socket.onopen = socketOnOpen;
};


init = function() {

  createSocket();

};


$(document).ready(function(){
  $('button.click')[0].addEventListener('touchstart', function(e) {
    e.preventDefault();
    click = true;
  }, false);

  $('button.click')[0].addEventListener('touchend', function(e) {
    e.preventDefault();
    click = false;
  }, false);

  $('#id-form').submit(function(e) {
    socket_url = socket_url + $('#desktop-id').val() + '/';
    $('#id-form').remove();
    init();
    e.preventDefault();
  });

});
