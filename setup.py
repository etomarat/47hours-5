# coding: utf-8

from setuptools import setup, find_packages


__version__ = '0.0.1a0'


requirements = [
    'tornado==4.1',
    ]

setup(
    name='ScreenShakeMotion',
    version=__version__,
    description='47Hours #5 project',
    author='Green Kun',
    author_email='solutio.sciurus@gmail.com',
    url='https://bitbucket.org/etomarat/47hours-5/',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=True,
    install_requires=requirements,
    entry_points={
        'console_scripts': [
            'ss-motion = server.server:main'
            ],
        },
    )

# EOF